namespace :reload do

  desc 'Relaod appointments for each patients on every first day of a month.'
  task appointments: :environment do
    Patient.all.each do |p|
      assistant_id = p.appointments.last.assistant_id
      app = p.appointments.create!(assistant_id: assistant_id)
    end
    puts "Appointments reloaded!"
  end
end