require 'test_helper'

class ControlchartsControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get controlcharts_create_url
    assert_response :success
  end

  test "should get new" do
    get controlcharts_new_url
    assert_response :success
  end

  test "should get show" do
    get controlcharts_show_url
    assert_response :success
  end

end
