Rails.application.routes.draw do


  #Assistant
  resources :assistants, only: [:create, :index, :update, :edit]

  get '/searchpatient' => 'assistants#searchpatient'
  get '/billing' => 'assistants#billing'
  get '/reports' => 'assistants#reports'
  get '/billing/today' => 'assistants#billing_today'
  get '/billing/currentmonth' => 'assistants#billing_currentmonth'
  get '/enroll' => 'assistants#enroll'
  get '/assistantsdashboard' =>'assistants#dashboard'
  get 'assistant/patientlist'=>'assistants#patientlist'
  get 'assistant/downloads'=>'assistants#downloads'
  get 'assistant/callcentre'=>'assistants#callcentre'
  get 'assistant/callcentre/currentmonth' => 'assistants#currentmonth'
  get 'assistant/callcentre/today' => 'assistants#today'
  get 'assistant/billedpatient/edit' => 'assistants#edit_billed'

  # get '/search' => 'assistants#search', as: 'search'

  # get 'patient/:id/profile' => 'assistants#patientprofile', as: 'profile'
    get 'patient/create_dm' => 'assistants#create_dm'
    get 'patient/create_htn' => 'assistants#create_htn'
  #Create Plans
  # resources :careplans
  scope path: "/patient/:id", as: 'patient' do
    get '/create_copd' => 'assistants#create_copd'
    get '/create_asthema' => 'assistants#create_asthema'
    get '/create_obesity' => 'assistants#create_obesity'
    get '/create_hf' => 'assistants#create_hf'
    get '/create_mh' => 'assistants#create_mh'
    get '/create_sh' => 'assistants#create_sh'
  end

  #patient routes
  get '/select_dm' => 'patients#select_dm'
  get '/patientsdashboard'=>'patients#dashboard'
  resources :patients, only: [:destroy, :edit, :update ,:create, :show] do 
    resources :careplans, only: [:create, :new, :edit, :update]
    resources :controlcharts, only: [:create, :new]
  end

  #Appointments
  resources :appointments, only: [:update]
  get '/bill_this_patient' => 'appointments#bill_this_patient'

  #doctor routes
  get '/doctorsdashboard' =>'doctors#dashboard'
  get 'doctors/patientlist' =>'doctors#patientlist'
  get 'doctors/careteam' =>'doctors#careteam'
  resources :careplanners, only: [:create, :index]


  #devise routes
  devise_for :users
  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  #extras
  get 'abc/index'
  root 'doctors#notfound'

  #active admin routes
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
