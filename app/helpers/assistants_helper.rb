module AssistantsHelper

  def time_box_equal_and_greater_than_20(mins)
    mins < "00:20:00" ? "badge badge-danger rounded" : "badge badge-success rounded"
  end

  def query_params(params)
    {
      member_id:   params[:member_id],
      name:        params[:name],
      dob:         params[:dob],
      created_at:  params[:created_at],
      insurance:   params[:insurance],
      last_call:   params[:last_call],
      care_plan:   params[:care_plan],
      total_mins:  params[:total_mins],
      eligibility: params[:eligibility],
      bill_status: params[:bill_status],
    }
  end

  def risk_factor_color(rf)
    (rf.eql? "HRF") ? "circle_high_risk_factor" : "circle_low_risk_factor"
  end

end
