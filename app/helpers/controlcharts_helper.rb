module ControlchartsHelper

  def bp_color_range_selector(bp)
    if (120..129).include?(bp.split('/')[0].to_i) && (80..84).include?(bp.split('/')[1].to_i)
      "label-success"
    elsif (130..149).include?(bp.split('/')[0].to_i) && (85..94).include?(bp.split('/')[1].to_i)
      "label-warning"
    elsif bp.split('/')[0].to_i > 150 && bp.split('/')[1].to_i > 95
      "label-danger"
    else
      "label-danger"      
    end
  end   

  def a1c_color_range_selector(a1c)
    if a1c > "8.0"
      "label-danger"
    elsif a1c < "7.0"
      "label-success"
    else
      "label-warning"
    end
  end   

  def ldl_color_range_selector(ldl)
    if ldl.to_i <= "99".to_i 
      "label-success"
    elsif (100..159).include?(ldl.to_i)
      "label-warning"
    elsif ldl.to_i > "160".to_i
      "label-danger"
    end
  end 

end
