module AppointmentsHelper

  def date_to_seconds(data)
    time = data.split(":")
    hur = time[0]
    min = time[1]
    sec = time[2]
    result = sec.to_i + (min.to_i*60) + (hur.to_i*60*60)
    result
  end

  def seconds_to_date(data)
    hur = data/(60*60)
    temp = data%3600
    min = temp/60
    sec = temp%60
    if (sec < 10)
      sec = '0'+sec.to_s
    end
    if min < 10
      min = '0'+min.to_s
    end
    if hur < 10
      hur = '0'+hur.to_s
    end 
    time = hur.to_s+":"+min.to_s+":"+sec.to_s
    time
  end

  def get_total_time(data1,data2)
    today = date_to_seconds data1
    total = date_to_seconds data2
    result = seconds_to_date(today + total)
    result
  end

  def get_total_today_mins(data1,data2)
    todayn = date_to_seconds data1
    totalp = date_to_seconds data2
    result = seconds_to_date(todayn + totalp)
    result
  end

end
