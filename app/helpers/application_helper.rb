module ApplicationHelper

  def today_calls_count
    current_user.today_total_patient_calls || 0
  end

  def monthly_calls_count
    current_user.monthly_total_patient_calls || 0
  end

  def today_billed_patients_count
    current_user.total_today_billed_patients || 0
  end

  def monthly_billed_patients_count
    current_user.total_current_month_billed_patients || 0
  end

  def to_date_format(date)
    date.strftime("%m/%d/%Y") if date.present?
  end

  def today_call_min(appointment)
    today = '00:00:00'
    today = appointment.today_mins if appointment.updated_at.to_date == Date.today
    return today
  end

  def patients_created_care_plans(plans)
    result = []
    plans.each do |plan|
      result << "DM" if plan.plan_type.eql? "Diabetic Care Plan"
      result << "HTN" if plan.plan_type.eql? "Hypertention Care Plan"
      result << "COPD" if plan.plan_type.eql? "COPD Care Plan"
      result << "ASTHMA" if plan.plan_type.eql? "ASTHMA Care Plan"
      result << "OBESITY" if plan.plan_type.eql? "OBESITY Care Plan"
      result << "HF" if plan.plan_type.eql? "Heart Faliure Care Plan"
      result << "MH" if plan.plan_type.eql? "Mental Health Care Plan"
      result << "SHD" if plan.plan_type.eql? "Schaemic Health Disease Care Plan"
    end
    return result.uniq
  end
  
end
