module CareplansHelper

  def checkbox_values_setter(plan,param)
    plan.barriers  = param[:bariers]  if param[:bariers] 
    plan.care_team_reporting  = param[:careteam_reporting] if param[:careteam_reporting]
    plan.other_treatment  = param[:other] if param[:other]
    plan.patient_education  = param[:patient_education] if param[:patient_education]
    plan.patient_preferences  = param[:patient_pref] if param[:patient_pref]
    plan.self_care_plan  = param[:self_plan] if param[:self_plan]
    plan.strategy_for_barriers  = param[:strategy_bariers] if param[:strategy_bariers]
    plan.treatment_goals  = param[:goals] if param[:goals]

    plan.comorbidity = param[:careplan][:comorbidity] if param[:careplan][:comorbidity]
    plan.diagnosis_primary = param[:careplan][:diagnosis_primary] if param[:careplan][:diagnosis_primary]
    plan.current_problems = param[:careplan][:current_problems] if param[:careplan][:current_problems]
    plan.current_medical_treatment = param[:careplan][:current_medical_treatment] if param[:careplan][:current_medical_treatment]
    plan.contact = param[:careplan][:contact] if param[:careplan][:contact]
    plan.signature = param[:careplan][:signature] if param[:careplan][:signature]
    plan
  end

  def careplan_details(careplan)
    goals = careplan.treatment_goals ? careplan.treatment_goals : []
    barriers = careplan.barriers ? careplan.barriers : []
    stretegy_barriers = careplan.strategy_for_barriers ? careplan.strategy_for_barriers : []
    pat_edu = careplan.patient_education ? careplan.patient_education : []
    pat_pref =careplan.patient_preferences ? careplan.patient_preferences : []
    selfplan =careplan.self_care_plan ? careplan.self_care_plan : []
    other = careplan.other_treatment ? careplan.other_treatment : []
    careteam =careplan.care_team_reporting ? careplan.care_team_reporting : []
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end

  def select_dm_careplan_details(careplan)
    goals = (careplan.treatment_goals.empty?) ?  (Careplan::GOALS) : (careplan.treatment_goals)
    barriers = (careplan.barriers.empty?) ?  (Careplan::BARIERS) : (careplan.barriers)
    stretegy_barriers = (careplan.strategy_for_barriers.empty?) ?  (Careplan::STRATEGY_BARIERS) : (careplan.strategy_for_barriers)
    pat_edu = (careplan.patient_education.empty?) ?  (Careplan::PATIENT_EDUCATION) : (careplan.patient_education)
    pat_pref = (careplan.patient_preferences.empty?) ?  (Careplan::PATIENT_PREF) : (careplan.patient_preferences)
    selfplan = (careplan.self_care_plan.empty?) ?  (Careplan::SELF_PLAN) : (careplan.self_care_plan)
    other = (careplan.other_treatment.empty?) ?  (Careplan::OTHER) : (careplan.other_treatment)
    careteam = (careplan.care_team_reporting.empty?) ?  (Careplan::CARETEAM_REPORTING) : (careplan.care_team_reporting)
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end
  
  def select_htn_careplan_details(careplan)
    goals = (careplan.treatment_goals.empty?) ?  (Careplan::HTN_GOALS) : (careplan.treatment_goals)
    barriers = (careplan.barriers.empty?) ?  (Careplan::HTN_BARIERS) : (careplan.barriers)
    stretegy_barriers = (careplan.strategy_for_barriers.empty?) ?  (Careplan::HTN_STRATEGY_BARIERS) : (careplan.strategy_for_barriers)
    pat_edu = (careplan.patient_education.empty?) ?  (Careplan::HTN_PATIENT_EDUCATION) : (careplan.patient_education)
    pat_pref = (careplan.patient_preferences.empty?) ?  (Careplan::HTN_PATIENT_PREF) : (careplan.patient_preferences)
    selfplan = (careplan.self_care_plan.empty?) ?  (Careplan::HTN_SELF_PLAN) : (careplan.self_care_plan)
    other = (careplan.other_treatment.empty?) ?  (Careplan::HTN_OTHER) : (careplan.other_treatment)
    careteam = (careplan.care_team_reporting.empty?) ?  (Careplan::HTN_CARETEAM_REPORTING) : (careplan.care_team_reporting)
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end
  
  def select_copd_careplan_details(careplan)
    goals = (careplan.treatment_goals.empty?) ?  (Careplan::COPD_GOALS) : (careplan.treatment_goals)
    barriers = (careplan.barriers.empty?) ?  (Careplan::COPD_BARIERS) : (careplan.barriers)
    stretegy_barriers = (careplan.strategy_for_barriers.empty?) ?  (Careplan::COPD_STRATEGY_BARIERS) : (careplan.strategy_for_barriers)
    pat_edu = (careplan.patient_education.empty?) ?  (Careplan::COPD_PATIENT_EDUCATION) : (careplan.patient_education)
    pat_pref = (careplan.patient_preferences.empty?) ?  (Careplan::COPD_PATIENT_PREF) : (careplan.patient_preferences)
    selfplan = (careplan.self_care_plan.empty?) ?  (Careplan::COPD_SELF_PLAN) : (careplan.self_care_plan)
    other = (careplan.other_treatment.empty?) ?  (Careplan::COPD_OTHER) : (careplan.other_treatment)
    careteam = (careplan.care_team_reporting.empty?) ?  (Careplan::COPD_CARETEAM_REPORTING) : (careplan.care_team_reporting)
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end
  
  def select_asthma_careplan_details(careplan)
    goals = (careplan.treatment_goals.empty?) ?  (Careplan::ASTHMA_GOALS) : (careplan.treatment_goals)
    barriers = (careplan.barriers.empty?) ?  (Careplan::ASTHMA_BARIERS) : (careplan.barriers)
    stretegy_barriers = (careplan.strategy_for_barriers.empty?) ?  (Careplan::ASTHMA_STRATEGY_BARIERS) : (careplan.strategy_for_barriers)
    pat_edu = (careplan.patient_education.empty?) ?  (Careplan::ASTHMA_PATIENT_EDUCATION) : (careplan.patient_education)
    pat_pref = (careplan.patient_preferences.empty?) ?  (Careplan::ASTHMA_PATIENT_PREF) : (careplan.patient_preferences)
    selfplan = (careplan.self_care_plan.empty?) ?  (Careplan::ASTHMA_SELF_PLAN) : (careplan.self_care_plan)
    other = (careplan.other_treatment.empty?) ?  (Careplan::ASTHMA_OTHER) : (careplan.other_treatment)
    careteam = (careplan.care_team_reporting.empty?) ?  (Careplan::ASTHMA_CARETEAM_REPORTING) : (careplan.care_team_reporting)
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end
  
  def select_obesity_careplan_details(careplan)
    goals = (careplan.treatment_goals.empty?) ?  (Careplan::OBESITY_GOALS) : (careplan.treatment_goals)
    barriers = (careplan.barriers.empty?) ?  (Careplan::OBESITY_BARIERS) : (careplan.barriers)
    stretegy_barriers = (careplan.strategy_for_barriers.empty?) ?  (Careplan::OBESITY_STRATEGY_BARIERS) : (careplan.strategy_for_barriers)
    pat_edu = (careplan.patient_education.empty?) ?  (Careplan::OBESITY_PATIENT_EDUCATION) : (careplan.patient_education)
    pat_pref = (careplan.patient_preferences.empty?) ?  (Careplan::OBESITY_PATIENT_PREF) : (careplan.patient_preferences)
    selfplan = (careplan.self_care_plan.empty?) ?  (Careplan::OBESITY_SELF_PLAN) : (careplan.self_care_plan)
    other = (careplan.other_treatment.empty?) ?  (Careplan::OBESITY_OTHER) : (careplan.other_treatment)
    careteam = (careplan.care_team_reporting.empty?) ?  (Careplan::OBESITY_CARETEAM_REPORTING) : (careplan.care_team_reporting)
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end
  
  def select_hf_careplan_details(careplan)
    goals = (careplan.treatment_goals.empty?) ?  (Careplan::HF_GOALS) : (careplan.treatment_goals)
    barriers = (careplan.barriers.empty?) ?  (Careplan::HF_BARIERS) : (careplan.barriers)
    stretegy_barriers = (careplan.strategy_for_barriers.empty?) ?  (Careplan::HF_STRATEGY_BARIERS) : (careplan.strategy_for_barriers)
    pat_edu = (careplan.patient_education.empty?) ?  (Careplan::HF_PATIENT_EDUCATION) : (careplan.patient_education)
    pat_pref = (careplan.patient_preferences.empty?) ?  (Careplan::HF_PATIENT_PREF) : (careplan.patient_preferences)
    selfplan = (careplan.self_care_plan.empty?) ?  (Careplan::HF_SELF_PLAN) : (careplan.self_care_plan)
    other = (careplan.other_treatment.empty?) ?  (Careplan::HF_OTHER) : (careplan.other_treatment)
    careteam = (careplan.care_team_reporting.empty?) ?  (Careplan::HF_CARETEAM_REPORTING) : (careplan.care_team_reporting)
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end
  
  def select_mh_careplan_details(careplan)
    goals = (careplan.treatment_goals.empty?) ?  (Careplan::MH_GOALS) : (careplan.treatment_goals)
    barriers = (careplan.barriers.empty?) ?  (Careplan::MH_BARIERS) : (careplan.barriers)
    stretegy_barriers = (careplan.strategy_for_barriers.empty?) ?  (Careplan::MH_STRATEGY_BARIERS) : (careplan.strategy_for_barriers)
    pat_edu = (careplan.patient_education.empty?) ?  (Careplan::MH_PATIENT_EDUCATION) : (careplan.patient_education)
    pat_pref = (careplan.patient_preferences.empty?) ?  (Careplan::MH_PATIENT_PREF) : (careplan.patient_preferences)
    selfplan = (careplan.self_care_plan.empty?) ?  (Careplan::MH_SELF_PLAN) : (careplan.self_care_plan)
    other = (careplan.other_treatment.empty?) ?  (Careplan::MH_OTHER) : (careplan.other_treatment)
    careteam = (careplan.care_team_reporting.empty?) ?  (Careplan::MH_CARETEAM_REPORTING) : (careplan.care_team_reporting)
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end
  
  def select_shd_careplan_details(careplan)
    goals = (careplan.treatment_goals.empty?) ?  (Careplan::SHD_GOALS) : (careplan.treatment_goals)
    barriers = (careplan.barriers.empty?) ?  (Careplan::SHD_BARIERS) : (careplan.barriers)
    stretegy_barriers = (careplan.strategy_for_barriers.empty?) ?  (Careplan::SHD_STRATEGY_BARIERS) : (careplan.strategy_for_barriers)
    pat_edu = (careplan.patient_education.empty?) ?  (Careplan::SHD_PATIENT_EDUCATION) : (careplan.patient_education)
    pat_pref = (careplan.patient_preferences.empty?) ?  (Careplan::SHD_PATIENT_PREF) : (careplan.patient_preferences)
    selfplan = (careplan.self_care_plan.empty?) ?  (Careplan::SHD_SELF_PLAN) : (careplan.self_care_plan)
    other = (careplan.other_treatment.empty?) ?  (Careplan::SHD_OTHER) : (careplan.other_treatment)
    careteam = (careplan.care_team_reporting.empty?) ?  (Careplan::SHD_CARETEAM_REPORTING) : (careplan.care_team_reporting)
    response = {goals: goals, barriers: barriers, stretegy_barriers: stretegy_barriers, pat_edu: pat_edu, pat_pref: pat_pref, selfplan: selfplan, other: other, careteam: careteam }
  end
end
