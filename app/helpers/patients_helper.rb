module PatientsHelper
  
  def get_care_plans(plans)
    plans = plans.join('-') if plans
  end

  def checked_care_plans(patient, care_plan)
    "checked" if patient.care_plan.include?(care_plan)
  end

end
