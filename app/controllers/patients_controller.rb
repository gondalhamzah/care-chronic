class PatientsController < ApplicationController
  include PatientsHelper
  before_action :get_patient, only: [:edit, :update, :destroy, :show]
  load_and_authorize_resource
  
  def dashboard
    @patient = current_user
    @plans = @patient.careplans
    @appointments = @patient.appointments.order("created_at asc")
    @billed_appoinments = @patient.billed_appoinments
  end

  def create
    @patient = Patient.new(patient_params)
    @patient.careplanner_id = current_user.id
    @patient.care_plan = params[:care_plan] if params[:care_plan]
    if @patient.save!
      @patient.appointments.create(assistant_id: current_user.id)
      flash[:notice] = "Patient Created"
      redirect_to assistantsdashboard_path
    else
      flash[:error] = "Unable to Create Patient"
      redirect_to enroll_path 
    end
  end

  def show
    @plans = @patient.careplans
    @appointments = @patient.appointments.order("created_at asc")
    @billed_appoinments = @patient.billed_appoinments
  end

  def edit
  end

  def select_dm
    @plan = Careplan.find_by_id params[:filter_for]
    
    respond_to do |format|
      format.js { render :nothing => true }
      format.html
    end
  end

  def update
    @patient.care_plan = params[:care_plan] if params[:care_plan]
    if @patient.update_attributes(patient_params)
      flash[:notice] = "Successfully updated Patient!"
      redirect_to assistant_patientlist_path
    else
      flash[:error] = "Error updating Patient!"
    end
  end

  def destroy
    if @patient.destroy
      flash[:notice] = "Successfully deleted Patient!"
      redirect_to assistant_patientlist_path
    else
      flash[:error] = "Error Deleting Patient!"
    end
  end

  private
  def patient_params
    params.require(:patient).permit!
  end

  def get_patient
    @patient = Patient.find(params[:id])
  end
end