class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def after_sign_in_path_for(resource)

    if params[:admin_user]
      admin_root_path
    elsif params[:user] || current_user.present?
      user = User.find_by_email params[:user][:email] if params[:user]
      user = current_user if current_user.present?
      if user.type.eql? User.doctor || current_user.doctor?
        doctorsdashboard_path
      elsif user.type.eql? User.patient  || current_user.patient?
        patientsdashboard_path
      elsif user.type.eql? User.assistant || current_user.assistant?
        assistantsdashboard_path
      end
    end
  
  end
  
  rescue_from CanCan::AccessDenied do |exception|
      
      if not current_user.nil? 
        user = current_user
        if user.type.eql? User.doctor || current_user.doctor?
          redirect_to doctorsdashboard_path
        elsif user.type.eql? User.patient  || current_user.patient?
          redirect_to patientsdashboard_path
        elsif user.type.eql? User.assistant || current_user.assistant?
          redirect_to assistantsdashboard_path
        end
      else
          redirect_to new_user_session_path
      end
  end
end
