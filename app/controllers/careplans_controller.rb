class CareplansController < ApplicationController
  include CareplansHelper
  before_action :find_careplan, only: [:edit, :update, :show, :delete]
  before_action :find_patient, only: [:edit, :update, :show, :new, :create]
  load_and_authorize_resource

  def index
    @careplans = Careplan.all
  end
  
  def create
    params[:careplan][:plan_type] = params[:plan_type].to_i
    @careplan = Careplan.new(careplan_params)
    param = checkbox_values_setter(@careplan,params)
    @careplan.patient_id = @patient.id

    if @careplan.save 
      flash[:notice] = "Successfully created Careplan!"
      redirect_to patient_path(@patient.id)
    else
      flash[:alert] = "Error creating new Careplan!"
    end
  end

  def edit
    case params['flag']
      when "Diabetic Care Plan" 
        result = careplan_details(@careplan)
        # result = select_dm_careplan_details(@careplan)
      when "Hypertention Care Plan" 
        result = careplan_details(@careplan)
        # result = select_htn_careplan_details(@careplan)
      when "COPD Care Plan" 
        result = careplan_details(@careplan)
        # result = select_copd_careplan_details(@careplan)
      when "ASTHMA Care Plan" 
        result = careplan_details(@careplan)
        # result = select_asthma_careplan_details(@careplan)
      when "OBESITY Care Plan" 
        result = careplan_details(@careplan)
        # result = select_obesity_careplan_details(@careplan)
      when "Heart Faliure Care Plan" 
        result = careplan_details(@careplan)
        # result = select_hf_careplan_details(@careplan)
      when "Mental Health Care Plan" 
        result = careplan_details(@careplan)
        # result = select_mh_careplan_details(@careplan)
      when "Schaemic Health Disease Care Plan" 
        result = careplan_details(@careplan)
        # result = select_shd_careplan_details(@careplan)
      else
        result = select_dm_careplan_details(@careplan)
    end
    @goals = result[:goals]
    @barriers = result[:barriers]
    @stretegy_barriers = result[:stretegy_barriers]
    @pat_edu = result[:pat_edu]
    @pat_pref = result[:pat_pref]
    @selfplan = result[:selfplan]
    @other = result[:other]
    @careteam = result[:careteam]
  end

  def update
    if @careplan.created_at.to_date.eql? Date.today
      params[:careplan][:plan_type] = params[:plan_type].to_i
      param = checkbox_values_setter(@careplan,params)
      if @careplan.update_attributes(careplan_params)
        flash[:notice] = "Successfully updated Careplan!"
        redirect_to patient_path(@patient.id)    
      else
        flash[:alert] = "Error updating Careplan!"
      end
    else
      @careplan = Careplan.new
      @careplan.plan_type = params[:plan_type].to_i
      param = checkbox_values_setter(@careplan,params)
      @careplan.patient_id = params[:patient_id].to_i
      if @careplan.save
        flash[:notice] = "Successfully updated Careplan!"
        redirect_to patient_path(@patient.id)    
      else
        flash[:alert] = "Error updating Careplan!"
      end
    end
  end

  def show
  end

  def destroy
    if @careplan.destroy
      flash[:notice] = "Successfully deleted Careplan!"
      # redirect_to posts_path
    else
      flash[:alert] = "Error deleting Careplan!"
    end
  end
  def new
    @careplan = Careplan.new  
  end

  private
  def careplan_params
    params.require(:careplan).permit!
  end

  def find_careplan
    @careplan = Careplan.find(params[:id])
  end

  def find_patient
    @patient = Patient.find(params[:patient_id])
  end

end