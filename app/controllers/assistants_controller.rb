class AssistantsController < ApplicationController
  include AssistantsHelper
  
  before_action :get_careplan, only: [:create_dm, :create_htn]
  load_and_authorize_resource
  
  def dashboard
  end

  def create
    if params[:password].eql? params[:password_confirmation]
      @user = Assistant.create(assistant_params)
      if @user.save
        @alert = "Care Planner created"
        flash[:notice] = "CarePlanner Created"
        redirect_to doctors_careteam_path
      else
        redirect_to root_path        
      end
    else
      flash[:alert] = "Password did not match unable to create CarePlanner"
      redirect_to doctors_careteam_path
    end
  end

  def edit
  end

  def update
    if params[:password].eql? params[:password_confirmation]
      @user = Assistant.find_by_id params[:id]
      if @user.update(update_params)
        @alert = "Care Planner Updated"
        flash[:notice] = "CarePlanner Updated"
        redirect_to doctors_careteam_path
      else
        redirect_to root_path        
      end
    else
      flash[:alert] = "Password did not match unable to create CarePlanner"
      redirect_to edit_assistant_path(@user)
    end
  end

  def edit_billed
    @appointment = Appointment.find_by_id params[:key]
    if @appointment.update(status_flag: false)
      flash[:notice] = "Sucessfully Created"
      respond_to do |format|
        format.js { render :nothing => true }
      end
    else
      flash[:alert] = "Unable to Update"
    end
  end

  def enroll
    @patient = Patient.new
  end

  def searchpatient
    @patients = nil
    @patients = current_user.filtered_total_patients(params) if params[:search] || params[:format]
    respond_to do |format|
      format.html
      format.csv { send_data @patients.to_csv, filename: "Search-Record-#{Date.today}.csv"}
    end
  end

  def patientlist
    @patients = current_user.total_patients
  end

  def reports
  end

  def billing
    @status_values = ["Received", "Pending"]
    @patients = current_user.eligible_patients_for_billing
  end

  def billing_today
    @patients = current_user.today_billed_patients
  end

  def billing_currentmonth
    @patients = current_user.current_month_billed_patients
  end


  def callcentre
    @patients = current_user.total_patients_under_20_min_call
  end

  def today
    @patients = current_user.today_patient_calls_list
  end

  def currentmonth
    @patients = current_user.monthly_patient_calls_list
  end

  def downloads
  end



  def create_dm
    
    render "assistants/careplans/dm_form"
  end

  def create_htn
    render "assistants/careplans/htn_form"
  end
  
  def create_copd
    render "assistants/careplans/copd_form"
  end
  
  def create_asthema
    render "assistants/careplans/asthema_form"
  end
  
  def create_obesity
    render "assistants/careplans/obesity_form"
  end
  
  def create_hf
    render "assistants/careplans/hf_form"
  end
  
  def create_mh
    render "assistants/careplans/mh_form"
  end

  def create_sh
    render "assistants/careplans/sh_form"
  end


  private
  def assistant_params
    params.require(:assistant).permit(:email, :password, :password_confirmation, :first_name, :last, :doctor_id)
  end

  def update_params
    params.require(:assistant).permit(:email, :password, :password_confirmation, :first_name, :last, :doctor_id)  
  end

  def my_patients
    # Patient.where(careplanner_id: current_user.id)
  end

  def search_params
    params.permit(:utf8, :commit, q: [:title_eq]).to_h
  end

  def get_careplan
    @careplan = Careplan.new
  end
end
