class ControlchartsController < ApplicationController
  include ControlchartsHelper
  before_action :find_patient, only: [:new, :create]

  def new
    @controlchart = Controlchart.new
  end

  def create
    @controlchart = Controlchart.new(chart_params)
    @controlchart.patient_id = @patient.id

    if @controlchart.save
      flash[:notice] = "Successfully created Chart!"
      redirect_to patient_path(@patient.id)
    else
      flash[:alert] = "Error Creating Control Chart"
      render 'new'
    end
  end

  private
  def find_patient
    @patient = Patient.find(params[:patient_id])
  end
  def chart_params
    params.require(:controlchart).permit!
  end
end
