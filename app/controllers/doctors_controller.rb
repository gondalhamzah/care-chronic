class DoctorsController < ApplicationController
  
  load_and_authorize_resource
  
  def dashboard
  end

  def careteam
    @assistant = Assistant.new
  end

  def patientlist
    @patients = current_user.assistant.total_patients
  end

  def notfound
  end

  private
  def assistant_params
  #  params.require(:assistant).permit(:email,:password,:cpassword,:first_name,:last)
  end
end
