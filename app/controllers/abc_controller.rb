class AbcController < ApplicationController
  before_action :authenticate_user!, only: [:index]

  def index
    @patient = Patient.new
  end
end
