class AppointmentsController < ApplicationController
  include AppointmentsHelper

  before_action :appoint_params, only: [:create, :update]

  def update
    @appoint = Appointment.find(params[:id])
    if (params[:_method] == "patch" && params[:appointment][:today_mins] > "00:00:00")
      @log = @appoint.timelogs.new(category: params['category'])
      @log.notes =  (params[:appointment][:notes] ? params[:appointment][:notes] : "")
      @log.call_time = (params[:appointment][:today_mins] ? params[:appointment][:today_mins] : "00:00:00")
      if not params[:appointment][:total_mins] == "00:00:00" 
        time = get_total_time(params[:appointment][:today_mins],@appoint.total_mins)
        params[:appointment][:total_mins] = time
      else
        params[:appointment][:total_mins] = params[:appointment][:today_mins]
      end
      @appoint.today_mins = "00:00:00" if @appoint.last_call != Date.today
      today = get_total_today_mins(@appoint.today_mins ,params[:appointment][:today_mins])
      params[:appointment][:today_mins] = today

      #Calling
      respond_to do |format|
        if @appoint.update(appoint_params)
          @log.save
          flash[:notice] = "Sucessfully Created"
          format.js { render :nothing => true }
          format.html
        else
          flash[:alert] = "Unable to Save"
        end
      end
    elsif (params[:_method] == "put")
      #Billing
      @appoint.status_flag = true
      respond_to do |format|
        if @appoint.update(appoint_params)
          flash[:notice] = "Sucessfully Created"
          format.js { render :nothing => true }
          format.html
        else
          flash[:alert] = "Unable to Save"
        end
      end
    end
  
  end

  def destroy
  end

  def bill_this_patient
    if Appointment.find_by_id(params[:appointment_id]).update(bill_flag: false)
      response = {flag: true}
    else
      response = {flag: false}
    end
  end

  private
  def appoint_params
    params.require(:appointment).permit!
  end
end
