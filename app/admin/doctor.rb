ActiveAdmin.register Doctor do


  permit_params :practice_name, :first_name, :last, :phone, :address, :email, :password, :password_confirmation

 controller do
    def create
      params.permit!
      user = Doctor.new(params[:doctor])
      if user.save
        redirect_to admin_doctors_path
      else
        redirect_to new_admin_doctor_path
      end
    end
  end

  form title: 'Add Practice'  do |f|
    inputs "Form" do
      input :practice_name, label: "Practice Name"
      input :first_name, label: "Owner's First Name"
      input :last , label: "Owner's Last Name"
      input :phone, label: "Practice Contact Number"
      input :address, label: "Practice Address"
      input :email, label: "Email"
      input :password , label: "Password"
      input :password_confirmation , label: "Confirm Password"
    end
    actions
  end
    
  index do
    column :practice_name
    column :first_name
    column :last
    column :address
    column :email
    column :phone
    column :type
    actions
  end

end