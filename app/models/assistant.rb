class Assistant < User
  
  validates_presence_of :first_name
  validates_presence_of :last

  belongs_to :doctor, class_name: "Doctor"
  has_many :patients, class_name: "Patient", foreign_key: "careplanner_id"
  has_many :appointments, dependent: :destroy

  CARE_PLAN = [ 'DM','HTN','COPD','ASTHMA','Obesity','HF','MH','SHD']
  RISK_FACTOR = [ 'LRF' , 'HRF' ]
  LRF = [['99490', '99490']]
  HRF = [
          ['99487', '99487'],
          ['99489', '99489']
        ]

  def total_patients
    self.patients.includes(:appointments)
  end


  def from_this_month
    self.patients.where("created_at > ? AND created_at < ?", Time.now.beginning_of_month, Time.now.end_of_month)
  end

  def mins_filter(param_mins, patient)
    patient = (param_mins.eql? "0 min") ? patient.where("appointments.total_mins = ?", "00:00:00").references(:appointments) : patient
    patient = (param_mins.eql? "< 20 min") ? patient.where("appointments.total_mins < ?", "00:20:00").references(:appointments) : patient
    patient = (param_mins.eql? "20+ min") ? patient.where("appointments.total_mins > ?", "00:20:00").references(:appointments) : patient
    patient
  end

  def name_filter(param_name,patient)
    patient = (not param_name.empty?) ? patient.where("(first_name LIKE ?) OR (last LIKE ?) ", "%#{param_name}%", "%#{param_name}%" ).references(:appointments) : patient
    patient
  end

  def filtered_total_patients(params)
    patients = self.patients.includes(:appointments)
    patients = (params[:member_id] != "") ? patients.where(medical_record_no: params[:member_id]).references(:appointments) : patients
    patients = (params[:name] != "") ? name_filter(params[:name],patients) : patients
    patients = (params[:dob] != "") ? patients.where(dob: params[:dob]).references(:appointments) : patients
    patients = (params[:insurance] != "") ? patients.where(insurance: params[:insurance]).references(:appointments) : patients
    patients = (params[:total_mins] != "All mins") ? mins_filter(params[:total_mins], patients) : patients
    patients = (params[:last_call] != "") ? patients.where("appointments.last_call = ?", params[:last_call]).references(:appointments) : patients
    patients = (params[:created_at] != "") ? patients.where('(extract(month from appointments.created_at) = ? ) AND (extract(year from
     appointments.created_at) = ? ) ', params[:created_at].to_date.month, params[:created_at].to_date.year).references(:appointments) : patients
    patients = (params[:eligibility] != "") ? patients.where("appointments.eligibility = ?", params[:eligibility]).references(:appointments) : patients
    patients = (params[:bill_status] != "") ? patients.where("appointments.bill_status = ?", params[:bill_status]).references(:appointments) : patients
    patients = (params[:care_plan] != "") ? patients.where(" ? = ANY (care_plan) ", params[:care_plan]).references(:appointments) : patients
    patients
  end

  # For Calling Section
  def total_patients_under_20_min_call
    self.patients.includes(:appointments).where("appointments.bill_flag = ? AND appointments.total_mins >= ? AND appointments.created_at >= ? AND appointments.created_at <= ?", true, "00:00:00", Date.today.at_beginning_of_month, Date.today.at_beginning_of_month.next_month).references(:appointments)
  end

  def today_patient_calls_list
    self.patients.includes(:appointments)
    .where(appointments: { last_call: Date.today.strftime("%Y-%m-%d") })
  end

  def today_total_patient_calls
    today_patient_calls_list.count if today_patient_calls_list
  end

  def monthly_patient_calls_list
    self.patients.includes(:appointments)
    .where("appointments.last_call >= ? AND appointments.last_call < ?",
      Date.today.at_beginning_of_month, Date.today.at_beginning_of_month.next_month)
    .references(:appointments)
  end

  def monthly_total_patient_calls
    monthly_patient_calls_list.count if monthly_patient_calls_list
  end

  # For Billing Section
  def eligible_patients_for_billing
    self.patients.includes(:appointments)
    .where("appointments.total_mins >= ? AND appointments.bill_flag = ? AND
      appointments.status_flag = ?",
      "00:00:00", false, false)
    .references(:appointments)
  end

  def today_billed_patients
    self.patients.includes(:appointments)
    .where("appointments.total_mins >= ? AND
      appointments.bill_flag = ? AND
      appointments.status_flag = ? AND
      appointments.last_billed_date = ?",
      "00:00:00", false, true, Date.today)
    .references(:appointments)
  end

  def current_month_billed_patients
    self.patients.includes(:appointments)
    .where("appointments.last_billed_date >= ? AND
      appointments.status_flag = ? AND
      appointments.last_billed_date < ?",
      Date.today.at_beginning_of_month, true,
      Date.today.at_beginning_of_month.next_month)
    .references(:appointments)
  end

  def total_today_billed_patients
    today_billed_patients.count if today_billed_patients
  end

  def total_current_month_billed_patients
    current_month_billed_patients.count if current_month_billed_patients
  end
  
end