class Patient < User
  
  validates_presence_of :first_name
  validates_presence_of :last

  # has_and_belongs_to_many :careplans
  has_many :careplans
  belongs_to :careplanner, class_name: "Assistant"

  has_many :appointments, :dependent => :destroy

  has_many :controlcharts, :dependent => :destroy 

  def name
    "#{self.last} #{self.first_name}"
  end

  def total_dm_plans
    self.careplans.where(plan_type: 0)
  end

  def patient_plans(plan)
    self.careplans.where(plan_type: plan.plan_type)
  end
  
  def total_htn_plans
    self.careplans.where(plan_type: 1)
  end

  def total_copd_plans
    self.careplans.where(plan_type: 2)
  end

  def total_asthma_plans
    self.careplans.where(plan_type: 3)
  end

  def total_obs_plans
    self.careplans.where(plan_type: 4)
  end

  def total_hf_plans
    self.careplans.where(plan_type: 5)
  end

  def total_mh_plans
    self.careplans.where(plan_type: 6)
  end

  def total_shd_plans
    self.careplans.where(plan_type: 7)
  end

  def high_risk_factor
    flag = false
    (flag = true) if self.rf.eql? "HRF"
    return flag
  end
  
  def billed_appoinments
    self.appointments.where(bill_flag: true).order("created_at asc")
  end

  def self.csv_header
    [
      'Member ID',
      'Full Name',
      'Date of Birth',
      'Month',
      'Care Plan',
      'Insurance',
      'Bill Status',
      'Amount',
      'Eligibilty',
      'Time Log'
    ]
  end

  def self.csv_row_values(patient)
    appointment = patient.appointments.last if patient.appointments
    care_plans = patient.care_plan.join(", ") if patient.care_plan

    [
      patient.medical_record_no,
      patient.name,
      patient.dob,
      patient.created_at.strftime("%b, %Y"),
      care_plans,
      patient.insurance,
      appointment.bill_status,
      appointment.amount,
      appointment.eligibility,
      appointment.total_mins
    ]

  end

  def self.to_csv(options = {})
    CSV.generate(headers: true) do |csv|
      csv << self.csv_header
      all.each do |patient|
        csv << self.csv_row_values(patient)
      end
    end
  end

end