class Careplan < ApplicationRecord
  belongs_to :patient

  enum plan_type: ['Diabetic Care Plan','Hypertention Care Plan',
      'COPD Care Plan','ASTHMA Care Plan',
      'OBESITY Care Plan','Heart Faliure Care Plan',
      'Mental Health Care Plan','Schaemic Health Disease Care Plan'      
    ]
    
  def uniq_careplan_types
    self.pluck(:plan_type).uniq
  end

  def self.first_dm_plan
    self.where(plan_type: 0).first
  end  

  def self.first_htn_plan
    self.where(plan_type: 1).first
  end

  def self.first_default_plan(pl)
    self.where(plan_type: pl).last
  end

  def html_id
    return "htn" if self.plan_type.eql? "Hypertention Care Plan"
    return "dm" if self.plan_type.eql? "Diabetic Care Plan"
  end

  def self.htn_and_dm_plan
    self.select("DISTINCT ON (plan_type) *").order(:plan_type, "created_at desc")
  end


  #DM PLANS
  GOALS = [
    'Control Diabetes A1c"%" below 8',
    'Control Blood pressure to resting blood pressure of 135/85 or below',
    'Control LDL (if LDL is greater Than 100)',
    'Reduce Weight (give goal BMI amount ex 27 or reduction of 5)( if BMI is greater than 28)  '
  ]
  BARIERS = [
    'Poor Compliance with monitoring blood glucose monitoring',
    'Poor compliance with diet',
    'Poor memory',
    'No Barriers'
  ]
  STRATEGY_BARIERS = [
   'Consult Home Service for Assistance with ADLs',
    'Improved medication compliance by referring home health services.  ',
    'Improve medication compliance by reminder calls for refills',
    'Call by care team to remind patients to check blood glucose and note any glucose recordings',
    'Referral to Diabetic Educator',
    'Home health to monitor blood glucose',
    'Improve diet with home nutrition counseling'
  ]

  PATIENT_EDUCATION = [
    'Glucose Monitoring Education',
    'Foot monitoring Education',
    'Low carb dieting',
    'Low calorie dieting',
    'Tobacco cessation education'
  ]

  PATIENT_PREF = [
    'To live a healthy lifestyle  ',
    'To lose weight',
    'Increase Stamina',
    'Prevent having to take additional medicine',
    'Avoid blindness and amputation.  ',
    'Avoid Heart Disease.',
    'Avoid Insulin.',
    'To live a healthy lifestyle'
  ]

  SELF_PLAN = [
    'Given Diabetic Glucose Diary and told to monitor Blood Glucose monitoring.  ',
    'Go for a walk at lunch-time  ',
    'Carb Counting Diary and asked for patient to monitor blood write carbs and maintain to less than (30-40)carbs',
    'Given Diabetic Glucose Diary and told to monitor Blood Glucose monitoring.',
    'Patient is given tobacco craving diary and ask to note down, what and when his /her cravings are.',
    'Go for a walk at lunch-time',
    'Given Diabetic Glucose Diary and told to monitor Blood Glucose monitoringGet some exercise regulary',
    'Monitor intake calorie in diary if in diet to maintain less than 1800 calories per day'
  ]

  OTHER = [
    'Low carb diet,comprising of less than 30 gram carbs approximately',
    'Calorie reduction diet  ',
    'Low cholesterol diet  ',
    'Low fat diet',
    'Low salt diet',
    'Excercise 30 min 4x day or as tolerable',
    'Monitor glucose 3x day',
    'Monitor BP every 2 days',
    'Check body weight weekly'
  ]

  CARETEAM_REPORTING = [
    'Care plan given to patient',
    'Care plan given to family',
    'Care plan mailed to patient address.'
  ]

  #HTN PLANS
  HTN_GOALS = [
    'Control blood pressure to the normal range i.e 120/80mmHG',
    'Reduce Weight (BMI goal ≤ 27)',
    'Control LDL (LDL goal less than 100)',
    'Decrease risk of strokes and cardiovascular events 28)'
  ]
  HTN_BARIERS = [
    'Lack in consistent medical follow-ups',
    'Medication side effects and dosing',
    'Poor Compliance with medication',
    'Poor Compliance with monitoring blood pressure',
    'Poor compliance with diet',
    'Needs help with ADLs',
    'No barriers note',
  ]
  HTN_STRATEGY_BARIERS = [
    'Consult Home Service for Assistance with ADLs',
    'Improved medication compliance by referring home health services',
    'Home health to monitor blood pressure readings',
    'Improve medication compliance by reminder calls for refills',
    'Call by care team to remind patients to check blood pressure and note any glucose recordings',
    'Improve diet with home nutrition counseling',
    'Improve diet with home nutrition counseling.',
  ]
  HTN_PATIENT_PREF = [
    'To live a healthy lifestyle',
    'Lose extra weight',
    'Increase stamina',
    'Prevent having to take additional medicine',
    'Avoid heart disease',
    'Avoid Heart Disease.',
    'Avoid headache',
    'Avoid Fatigue',
    'Live longer',
  ]
  HTN_PATIENT_EDUCATION = [
    'How to avoid salt in regular diet',
    'Low calorie dieting training',
    'Tobacco cessation education',
    'Hypertension monitoring education'
  ]

  HTN_SELF_PLAN =[
    'Decrease the salt in daily diet or avoid',
    'Avoid eating red meat',
    'Practice relaxation or slow, deep breathing',
    'blood pressure diary and told to monitor blood pressure readings',
    'Patient is given tobacco craving diary and ask to note down, what and when his /her cravings are.',
    'Go for a walk at lunch-time',
    'Get some exercise regularly',
    'Increase physical activity'
  ]
  HTN_OTHER = [
    'Increase physical activity',
    'Diet low carb',
    'Diet comprising of less than 30 gram carbs',
    'Calorie reduction diet',
    'Low cholesterol diet',
    'Low fat diet',
    'Low salt diet',
    'Exercise 30 min',
    'Monitor glucose 3 x day',
    'Monitor BP every 2 days'
  ]
  HTN_CARETEAM_REPORTING = [
    'Care plan given to patient',
    'Care plan given to family',
    'Care plan mailed to patient address.'
  ]


  #ASTHMA PLANS
  ASTHMA_GOALS = [
    'Control blood pressure to the normal range i.e 120/80mmHG',
    'Reduce Weight (BMI goal ≤ 27)',
    'Control LDL (LDL goal less than 100)',
    'Decrease risk of strokes and cardiovascular events 28)'
  ]
  ASTHMA_BARIERS = [
    'Lack in consistent medical follow-ups',
    'Medication side effects and dosing',
    'Poor Compliance with medication',
    'Poor Compliance with monitoring blood pressure',
    'Poor compliance with diet',
    'Needs help with ADLs',
    'No barriers note',
  ]
  ASTHMA_STRATEGY_BARIERS = [
    'Consult Home Service for Assistance with ADLs',
    'Improved medication compliance by referring home health services',
    'Home health to monitor blood pressure readings',
    'Improve medication compliance by reminder calls for refills',
    'Call by care team to remind patients to check blood pressure and note any glucose recordings',
    'Improve diet with home nutrition counseling',
    'Improve diet with home nutrition counseling.',
  ]
  ASTHMA_PATIENT_PREF = [
    'To live a healthy lifestyle',
    'Lose extra weight',
    'Increase stamina',
    'Prevent having to take additional medicine',
    'Avoid heart disease',
    'Avoid Heart Disease.',
    'Avoid headache',
    'Avoid Fatigue',
    'Live longer',
  ]
  ASTHMA_PATIENT_EDUCATION = [
    'How to avoid salt in regular diet',
    'Low calorie dieting training',
    'Tobacco cessation education',
    'Hypertension monitoring education'
  ]

  ASTHMA_SELF_PLAN =[
    'Decrease the salt in daily diet or avoid',
    'Avoid eating red meat',
    'Practice relaxation or slow, deep breathing',
    'blood pressure diary and told to monitor blood pressure readings',
    'Patient is given tobacco craving diary and ask to note down, what and when his /her cravings are.',
    'Go for a walk at lunch-time',
    'Get some exercise regularly',
    'Increase physical activity'
  ]
  ASTHMA_OTHER = [
    'Increase physical activity',
    'Diet low carb',
    'Diet comprising of less than 30 gram carbs',
    'Calorie reduction diet',
    'Low cholesterol diet',
    'Low fat diet',
    'Low salt diet',
    'Exercise 30 min',
    'Monitor glucose 3 x day',
    'Monitor BP every 2 days'
  ]
  ASTHMA_CARETEAM_REPORTING = [
    'Care plan given to patient',
    'Care plan given to family',
    'Care plan mailed to patient address.'
  ]


  #COPD PLANS
  COPD_GOALS = [
    'Control blood pressure to the normal range i.e 120/80mmHG',
    'Reduce Weight (BMI goal ≤ 27)',
    'Control LDL (LDL goal less than 100)',
    'Decrease risk of strokes and cardiovascular events 28)'
  ]
  COPD_BARIERS = [
    'Lack in consistent medical follow-ups',
    'Medication side effects and dosing',
    'Poor Compliance with medication',
    'Poor Compliance with monitoring blood pressure',
    'Poor compliance with diet',
    'Needs help with ADLs',
    'No barriers note',
  ]
  COPD_STRATEGY_BARIERS = [
    'Consult Home Service for Assistance with ADLs',
    'Improved medication compliance by referring home health services',
    'Home health to monitor blood pressure readings',
    'Improve medication compliance by reminder calls for refills',
    'Call by care team to remind patients to check blood pressure and note any glucose recordings',
    'Improve diet with home nutrition counseling',
    'Improve diet with home nutrition counseling.',
  ]
  COPD_PATIENT_PREF = [
    'To live a healthy lifestyle',
    'Lose extra weight',
    'Increase stamina',
    'Prevent having to take additional medicine',
    'Avoid heart disease',
    'Avoid Heart Disease.',
    'Avoid headache',
    'Avoid Fatigue',
    'Live longer',
  ]
  COPD_PATIENT_EDUCATION = [
    'How to avoid salt in regular diet',
    'Low calorie dieting training',
    'Tobacco cessation education',
    'Hypertension monitoring education'
  ]

  COPD_SELF_PLAN =[
    'Decrease the salt in daily diet or avoid',
    'Avoid eating red meat',
    'Practice relaxation or slow, deep breathing',
    'blood pressure diary and told to monitor blood pressure readings',
    'Patient is given tobacco craving diary and ask to note down, what and when his /her cravings are.',
    'Go for a walk at lunch-time',
    'Get some exercise regularly',
    'Increase physical activity'
  ]
  COPD_OTHER = [
    'Increase physical activity',
    'Diet low carb',
    'Diet comprising of less than 30 gram carbs',
    'Calorie reduction diet',
    'Low cholesterol diet',
    'Low fat diet',
    'Low salt diet',
    'Exercise 30 min',
    'Monitor glucose 3 x day',
    'Monitor BP every 2 days'
  ]
  COPD_CARETEAM_REPORTING = [
    'Care plan given to patient',
    'Care plan given to family',
    'Care plan mailed to patient address.'
  ]

    #OBESITY PLANS
  OBESITY_GOALS = [
    'Control blood pressure to the normal range i.e 120/80mmHG',
    'Reduce Weight (BMI goal ≤ 27)',
    'Control LDL (LDL goal less than 100)',
    'Decrease risk of strokes and cardiovascular events 28)'
  ]
  OBESITY_BARIERS = [
    'Lack in consistent medical follow-ups',
    'Medication side effects and dosing',
    'Poor Compliance with medication',
    'Poor Compliance with monitoring blood pressure',
    'Poor compliance with diet',
    'Needs help with ADLs',
    'No barriers note',
  ]
  OBESITY_STRATEGY_BARIERS = [
    'Consult Home Service for Assistance with ADLs',
    'Improved medication compliance by referring home health services',
    'Home health to monitor blood pressure readings',
    'Improve medication compliance by reminder calls for refills',
    'Call by care team to remind patients to check blood pressure and note any glucose recordings',
    'Improve diet with home nutrition counseling',
    'Improve diet with home nutrition counseling.',
  ]
  OBESITY_PATIENT_PREF = [
    'To live a healthy lifestyle',
    'Lose extra weight',
    'Increase stamina',
    'Prevent having to take additional medicine',
    'Avoid heart disease',
    'Avoid Heart Disease.',
    'Avoid headache',
    'Avoid Fatigue',
    'Live longer',
  ]
  OBESITY_PATIENT_EDUCATION = [
    'How to avoid salt in regular diet',
    'Low calorie dieting training',
    'Tobacco cessation education',
    'Hypertension monitoring education'
  ]

  OBESITY_SELF_PLAN =[
    'Decrease the salt in daily diet or avoid',
    'Avoid eating red meat',
    'Practice relaxation or slow, deep breathing',
    'blood pressure diary and told to monitor blood pressure readings',
    'Patient is given tobacco craving diary and ask to note down, what and when his /her cravings are.',
    'Go for a walk at lunch-time',
    'Get some exercise regularly',
    'Increase physical activity'
  ]
  OBESITY_OTHER = [
    'Increase physical activity',
    'Diet low carb',
    'Diet comprising of less than 30 gram carbs',
    'Calorie reduction diet',
    'Low cholesterol diet',
    'Low fat diet',
    'Low salt diet',
    'Exercise 30 min',
    'Monitor glucose 3 x day',
    'Monitor BP every 2 days'
  ]
  OBESITY_CARETEAM_REPORTING = [
    'Care plan given to patient',
    'Care plan given to family',
    'Care plan mailed to patient address.'
  ]


    #MH PLANS
  MH_GOALS = [
    'Control blood pressure to the normal range i.e 120/80mmHG',
    'Reduce Weight (BMI goal ≤ 27)',
    'Control LDL (LDL goal less than 100)',
    'Decrease risk of strokes and cardiovascular events 28)'
  ]
  MH_BARIERS = [
    'Lack in consistent medical follow-ups',
    'Medication side effects and dosing',
    'Poor Compliance with medication',
    'Poor Compliance with monitoring blood pressure',
    'Poor compliance with diet',
    'Needs help with ADLs',
    'No barriers note',
  ]
  MH_STRATEGY_BARIERS = [
    'Consult Home Service for Assistance with ADLs',
    'Improved medication compliance by referring home health services',
    'Home health to monitor blood pressure readings',
    'Improve medication compliance by reminder calls for refills',
    'Call by care team to remind patients to check blood pressure and note any glucose recordings',
    'Improve diet with home nutrition counseling',
    'Improve diet with home nutrition counseling.',
  ]
  MH_PATIENT_PREF = [
    'To live a healthy lifestyle',
    'Lose extra weight',
    'Increase stamina',
    'Prevent having to take additional medicine',
    'Avoid heart disease',
    'Avoid Heart Disease.',
    'Avoid headache',
    'Avoid Fatigue',
    'Live longer',
  ]
  MH_PATIENT_EDUCATION = [
    'How to avoid salt in regular diet',
    'Low calorie dieting training',
    'Tobacco cessation education',
    'Hypertension monitoring education'
  ]

  MH_SELF_PLAN =[
    'Decrease the salt in daily diet or avoid',
    'Avoid eating red meat',
    'Practice relaxation or slow, deep breathing',
    'blood pressure diary and told to monitor blood pressure readings',
    'Patient is given tobacco craving diary and ask to note down, what and when his /her cravings are.',
    'Go for a walk at lunch-time',
    'Get some exercise regularly',
    'Increase physical activity'
  ]
  MH_OTHER = [
    'Increase physical activity',
    'Diet low carb',
    'Diet comprising of less than 30 gram carbs',
    'Calorie reduction diet',
    'Low cholesterol diet',
    'Low fat diet',
    'Low salt diet',
    'Exercise 30 min',
    'Monitor glucose 3 x day',
    'Monitor BP every 2 days'
  ]
  MH_CARETEAM_REPORTING = [
    'Care plan given to patient',
    'Care plan given to family',
    'Care plan mailed to patient address.'
  ]


    #SHD PLANS
  SHD_GOALS = [
    'Control blood pressure to the normal range i.e 120/80mmHG',
    'Reduce Weight (BMI goal ≤ 27)',
    'Control LDL (LDL goal less than 100)',
    'Decrease risk of strokes and cardiovascular events 28)'
  ]
  SHD_BARIERS = [
    'Lack in consistent medical follow-ups',
    'Medication side effects and dosing',
    'Poor Compliance with medication',
    'Poor Compliance with monitoring blood pressure',
    'Poor compliance with diet',
    'Needs help with ADLs',
    'No barriers note',
  ]
  SHD_STRATEGY_BARIERS = [
    'Consult Home Service for Assistance with ADLs',
    'Improved medication compliance by referring home health services',
    'Home health to monitor blood pressure readings',
    'Improve medication compliance by reminder calls for refills',
    'Call by care team to remind patients to check blood pressure and note any glucose recordings',
    'Improve diet with home nutrition counseling',
    'Improve diet with home nutrition counseling.',
  ]
  SHD_PATIENT_PREF = [
    'To live a healthy lifestyle',
    'Lose extra weight',
    'Increase stamina',
    'Prevent having to take additional medicine',
    'Avoid heart disease',
    'Avoid Heart Disease.',
    'Avoid headache',
    'Avoid Fatigue',
    'Live longer',
  ]
  SHD_PATIENT_EDUCATION = [
    'How to avoid salt in regular diet',
    'Low calorie dieting training',
    'Tobacco cessation education',
    'Hypertension monitoring education'
  ]

  SHD_SELF_PLAN =[
    'Decrease the salt in daily diet or avoid',
    'Avoid eating red meat',
    'Practice relaxation or slow, deep breathing',
    'blood pressure diary and told to monitor blood pressure readings',
    'Patient is given tobacco craving diary and ask to note down, what and when his /her cravings are.',
    'Go for a walk at lunch-time',
    'Get some exercise regularly',
    'Increase physical activity'
  ]
  SHD_OTHER = [
    'Increase physical activity',
    'Diet low carb',
    'Diet comprising of less than 30 gram carbs',
    'Calorie reduction diet',
    'Low cholesterol diet',
    'Low fat diet',
    'Low salt diet',
    'Exercise 30 min',
    'Monitor glucose 3 x day',
    'Monitor BP every 2 days'
  ]
  SHD_CARETEAM_REPORTING = [
    'Care plan given to patient',
    'Care plan given to family',
    'Care plan mailed to patient address.'
  ]



    #HF PLANS
  HF_GOALS = [
    'Control blood pressure to the normal range i.e 120/80mmHG',
    'Reduce Weight (BMI goal ≤ 27)',
    'Control LDL (LDL goal less than 100)',
    'Decrease risk of strokes and cardiovascular events 28)'
  ]
  HF_BARIERS = [
    'Lack in consistent medical follow-ups',
    'Medication side effects and dosing',
    'Poor Compliance with medication',
    'Poor Compliance with monitoring blood pressure',
    'Poor compliance with diet',
    'Needs help with ADLs',
    'No barriers note',
  ]
  HF_STRATEGY_BARIERS = [
    'Consult Home Service for Assistance with ADLs',
    'Improved medication compliance by referring home health services',
    'Home health to monitor blood pressure readings',
    'Improve medication compliance by reminder calls for refills',
    'Call by care team to remind patients to check blood pressure and note any glucose recordings',
    'Improve diet with home nutrition counseling',
    'Improve diet with home nutrition counseling.',
  ]
  HF_PATIENT_PREF = [
    'To live a healthy lifestyle',
    'Lose extra weight',
    'Increase stamina',
    'Prevent having to take additional medicine',
    'Avoid heart disease',
    'Avoid Heart Disease.',
    'Avoid headache',
    'Avoid Fatigue',
    'Live longer',
  ]
  HF_PATIENT_EDUCATION = [
    'How to avoid salt in regular diet',
    'Low calorie dieting training',
    'Tobacco cessation education',
    'Hypertension monitoring education'
  ]

  HF_SELF_PLAN =[
    'Decrease the salt in daily diet or avoid',
    'Avoid eating red meat',
    'Practice relaxation or slow, deep breathing',
    'blood pressure diary and told to monitor blood pressure readings',
    'Patient is given tobacco craving diary and ask to note down, what and when his /her cravings are.',
    'Go for a walk at lunch-time',
    'Get some exercise regularly',
    'Increase physical activity'
  ]
  HF_OTHER = [
    'Increase physical activity',
    'Diet low carb',
    'Diet comprising of less than 30 gram carbs',
    'Calorie reduction diet',
    'Low cholesterol diet',
    'Low fat diet',
    'Low salt diet',
    'Exercise 30 min',
    'Monitor glucose 3 x day',
    'Monitor BP every 2 days'
  ]
  HF_CARETEAM_REPORTING = [
    'Care plan given to patient',
    'Care plan given to family',
    'Care plan mailed to patient address.'
  ]
end
