class Timelog < ApplicationRecord
  belongs_to :appointment

  CATEGORIES = [
   "CCM Enrollment", "Care Plan Analysis", "Care Plan Review", "Care Coordination",
   "Care Transition - ER/UrgentCare", "Care Transition - Hospital",
   "Care Transition - SpcialistReferral", "Care Transition - Other",
   "Medication Management", "Patient Generated Data Review", 
   "Phone Call - Care Coordination", "Phone Call - Medication Management",
    "Phone Call - Patient Counseling", "Phone Call - Other"
  ]
end
