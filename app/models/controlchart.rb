class Controlchart < ApplicationRecord

  belongs_to :patient

  validate :at_least_one_name

  def at_least_one_name
    if [self.a1c, self.bp, self.ldl].reject(&:blank?).size == 0
      errors[:base] << ("Please choose at least one name - any language will do.")
    end
  end     
end
