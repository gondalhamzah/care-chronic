class Doctor < User

  validates_presence_of :first_name
  validates_presence_of :last
  # #one to one relation doctor and careplanner
  has_one :assistant, class_name: "Assistant", foreign_key: "doctor_id"

end