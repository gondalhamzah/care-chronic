class User < ApplicationRecord
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  
  #Get methods for UserType
  def self.doctor
    "Doctor"
  end
  
  def self.patient
    "Patient"
  end
  
  def self.assistant
    "Assistant"
  end

  def doctor?
    self.type.eql? "Doctor" 
  end

  def patient?
    self.type.eql? "Patient"
  end

  def planner?
    self.type.eql? "Assistant"
  end

  def full_name
    "#{self.first_name} #{self.last}"
  end

end