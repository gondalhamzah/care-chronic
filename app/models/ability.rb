class Ability
  include CanCan::Ability

  def initialize(user)

    if not user.nil?
        if user.doctor?
            can :dashboard, Doctor         
            can :manage , Assistant
            can :patientlist , Doctor
            can :careteam , Doctor if user.assistant.nil?
            can :create , Assistant
            can :edit , Assistant
            can :update , Assistant
            can :patientlist, Assistant   
            can :show , Patient
            
            cannot :dashboard, Patient
            cannot :dashboard, Assistant

        
        elsif user.planner?
            cannot :dashboard, Patient
            cannot :dashboard, Doctor
            can :dashboard, Assistant    
            can :callcentre, Assistant   
            can :enroll, Assistant   
            can :patientlist, Assistant   
            can :billing, Assistant   
            can :reports, Assistant   
            can :billing_today, Assistant
            can :patientprofile, Assistant
            can :billing_currentmonth, Assistant
            can :billing_today, Assistant
            can :downloads, Assistant
            can :today, Assistant
            can :currentmonth, Assistant 
            can :create_dm, Assistant 
            can :create_htn, Assistant  
            can :search, Assistant  
            can :destroy, Patient 
            can :update, Patient 
            can :edit, Patient 
            can :create, Patient
            can :show, Patient
            can :select_dm , Patient
            can :edit , Patient
            can :update , Patient
            can :searchpatient , Assistant
            can :edit_billed, Assistant

            can :edit , Careplan
            can :update , Careplan
            can :new , Careplan
            can :create , Careplan

            #appointment
            can :update, Appointment

        elsif user.patient?
            cannot :dashboard, Doctor
            cannot :dashboard, Assistant
            can :dashboard, Patient
            can :all, Patient
            cannot :create, Patient

        end
    end
    
  end
end
