class Appointment < ApplicationRecord
  belongs_to :patient
  belongs_to :assistant
  has_many :timelogs

  def total_mins_more_than_20
    if (self.total_mins.split(":")[0].to_i < 1) && (self.total_mins.split(":")[1].to_i > 20)
      return true
    else
      return false
    end
  end

  def self.today_appoinments(assistant_id)
    Appointment.joins(:assistant).where(
                                    last_call: Date.today.strftime("%Y-%m-%d"),
                                     assistant_id: assistant_id
                                     )
  end

  def self.total_today_appointments(assistant_id)
    self.today_appoinments(assistant_id).count
  end

end
