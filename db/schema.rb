# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171011185637) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "appointments", force: :cascade do |t|
    t.date "last_call"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "code"
    t.string "notes"
    t.string "today_mins", default: "00:00:00"
    t.string "total_mins", default: "00:00:00"
    t.text "eligibility"
    t.integer "amount"
    t.string "bill_status"
    t.string "bill_notes"
    t.boolean "bill_flag", default: false
    t.integer "patient_id"
    t.integer "assistant_id"
    t.date "last_billed_date"
  end

  create_table "careplans", force: :cascade do |t|
    t.text "current_problems"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "barriers", default: [], array: true
    t.string "care_team_reporting", default: [], array: true
    t.string "comorbidity"
    t.string "contact"
    t.text "current_medical_treatment"
    t.string "diagnosis_primary"
    t.string "medical_allergies"
    t.text "other_treatment", default: [], array: true
    t.text "patient_education", default: [], array: true
    t.text "patient_preferences", default: [], array: true
    t.text "providers"
    t.text "self_care_plan", default: [], array: true
    t.string "signature"
    t.text "strategy_for_barriers", default: [], array: true
    t.text "treatment_goals", default: [], array: true
    t.integer "plan_type"
    t.integer "patient_id"
  end

  create_table "controlcharts", force: :cascade do |t|
    t.string "a1c"
    t.string "bp"
    t.string "ldl"
    t.integer "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "type"
    t.string "first_name"
    t.string "last"
    t.string "age"
    t.string "care_team"
    t.text "clinical_manager"
    t.string "clinical_team_coordinator"
    t.date "date"
    t.string "dob"
    t.string "insurance"
    t.string "language_preference"
    t.string "ma"
    t.string "medical_record_no"
    t.string "sex"
    t.string "title"
    t.string "web_enabled"
    t.integer "careplanner_id"
    t.integer "doctor_id"
    t.string "care_plan", default: [], array: true
    t.string "phone"
    t.string "emergency_phone"
    t.string "practice_name"
    t.string "address"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
