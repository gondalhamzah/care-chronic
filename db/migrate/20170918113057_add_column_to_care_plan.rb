class AddColumnToCarePlan < ActiveRecord::Migration[5.1]
  def change
    add_column :careplans, :barriers, :text, array: true, default: []
    add_column :careplans, :care_team_reporting, :string, array: true, default: []
    add_column :careplans, :comorbidity, :string
    add_column :careplans, :contact, :string
    add_column :careplans, :current_medical_treatment, :text
    add_column :careplans, :diagnosis_primary, :string
    add_column :careplans, :medical_allergies, :string
    add_column :careplans, :other_treatment, :text, array: true, default: []
    add_column :careplans, :patient_education, :text, array: true, default: []
    add_column :careplans, :patient_preferences, :text, array: true, default: []
    add_column :careplans, :providers, :text
    add_column :careplans, :self_care_plan, :text, array: true, default: []
    add_column :careplans, :signature, :string
    add_column :careplans, :strategy_for_barriers, :text, array: true, default: []
    add_column :careplans, :treatment_goals, :text , array: true, default: []
    add_column :careplans, :plan_type ,:integer

    add_column :careplans, :patient_id ,:integer
  end
end
