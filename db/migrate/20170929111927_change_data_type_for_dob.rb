class ChangeDataTypeForDob < ActiveRecord::Migration[5.1]
  def change
    change_column :users, :dob, :string
  end
end
