class AddBilledStatusFlag < ActiveRecord::Migration[5.1]
  def change
    add_column :appointments, :status_flag, :boolean, default: false
  end
end
