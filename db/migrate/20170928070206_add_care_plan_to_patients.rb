class AddCarePlanToPatients < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :care_plan, :string , array: true, default: []
  end
end
