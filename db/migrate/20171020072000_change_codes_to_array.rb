class ChangeCodesToArray < ActiveRecord::Migration[5.1]
  def change
    change_column :appointments, :code, :string, array: true, default: [], using: "(string_to_array(code, ','))"
  end
end
