class AddModelRelations < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :careplanner_id, :integer
    add_column :users, :doctor_id, :integer
  end
end
