class AddAppointmentFields < ActiveRecord::Migration[5.1]
  def change
    add_column :appointments, :code, :string
    add_column :appointments, :notes, :string
    add_column :appointments, :today_mins, :string, default: "00:00:00"
    add_column :appointments, :total_mins, :string, default: "00:00:00"
    add_column :appointments, :eligibility, :text
    add_column :appointments, :amount, :integer
    add_column :appointments, :bill_status, :string
    add_column :appointments, :bill_notes, :string    
    add_column :appointments, :bill_flag, :boolean, default: false

    add_column :appointments, :patient_id, :integer    
  end
end
