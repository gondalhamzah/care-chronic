class AddLastBilledDateToAppointments < ActiveRecord::Migration[5.1]
  def change
    add_column :appointments, :last_billed_date, :date
  end
end
