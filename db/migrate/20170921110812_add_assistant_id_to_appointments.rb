class AddAssistantIdToAppointments < ActiveRecord::Migration[5.1]
  def change
    add_column :appointments, :assistant_id, :integer
  end
end
