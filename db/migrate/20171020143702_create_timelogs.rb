class CreateTimelogs < ActiveRecord::Migration[5.1]
  def change
    create_table :timelogs do |t|
      t.integer :category
      t.string :call_time
      t.text :notes
      t.integer :appointment_id

      t.timestamps
    end
  end
end
