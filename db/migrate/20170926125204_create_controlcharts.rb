class CreateControlcharts < ActiveRecord::Migration[5.1]
  def change
    create_table :controlcharts do |t|
      t.string :a1c
      t.string :bp
      t.string :ldl
      t.integer :patient_id

      t.timestamps
    end
  end
end
