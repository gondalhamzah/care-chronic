class CreateCareplans < ActiveRecord::Migration[5.1]
  def change
    create_table :careplans do |t|
      t.text :current_problems

      t.timestamps
    end
  end
end
