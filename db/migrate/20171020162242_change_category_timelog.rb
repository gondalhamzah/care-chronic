class ChangeCategoryTimelog < ActiveRecord::Migration[5.1]
  def change
    change_column :timelogs, :category, :string
  end
end