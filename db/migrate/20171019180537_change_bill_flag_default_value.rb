class ChangeBillFlagDefaultValue < ActiveRecord::Migration[5.1]
  def change
    change_column :appointments, :bill_flag, :boolean, default: true
  end
end
