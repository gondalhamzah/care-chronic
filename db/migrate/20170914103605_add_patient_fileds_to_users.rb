class AddPatientFiledsToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last, :name
    add_column :users, :age, :string
    add_column :users, :care_team, :string
    add_column :users, :clinical_manager, :text
    add_column :users, :clinical_team_coordinator, :string

    add_column :users, :date, :date
    add_column :users, :dob, :date
    add_column :users, :insurance, :string

    add_column :users, :language_preference, :string
    add_column :users, :ma, :string
    add_column :users, :medical_record_no, :string

    add_column :users, :sex, :string
    add_column :users, :title, :string
    add_column :users, :web_enabled, :string


  end
end
