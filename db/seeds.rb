# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development? || Rails.env.production?

# if Rails.env.development?
#   #1
  # Doctor.create(email: "a@a.com", first_name: "John",last: "Doe",password: "password", password_confirmation: "password")
  # Assistant.create(email: "b@a.com",first_name: "John",last: "Doe", password: "password", password_confirmation: "password", doctor_id: 1)
  # Patient.create(email: "c@a.com",first_name: "John",last: "Doe", password: "password", password_confirmation: "password", careplanner_id: 2, dob: Date.today- 20.years, age: "20", medical_record_no: "HJSUE2", insurance: "DawoodTakaful" )
#   2
#   Patient.create(email: "cc@a.com",first_name: "John2",last: "Doe2", password: "password", password_confirmation: "password", careplanner_id: 2, dob: Date.today- 20.years, age: "20", medical_record_no: "HJSUE2", insurance: "DawoodTakaful" )


  # Careplan.create(current_problems: "Many", barriers: "too many", care_team_reporting: "yeah ",comorbidity: "may be", contact: "0098877663737", current_medical_treatment: "on the way", diagnosis_primary: "diabtese", medical_allergies: "no", other_treatment: "no", patient_education: "masters", patient_preferences: " all", providers: "for what", self_care_plan: "first", signature: "name", strategy_for_barriers: "searching", treatment_goals: "perfect", patient_id: 3)

#   #1
#   Appointment.create(last_call: Date.today-2, code: ["99490"] ,notes: "Patient is good",bill_notes: "Patients bill Cleared", today_mins: "00:02:45", total_mins: "00:12:45", eligibility: "Eligible", amount: 32, bill_flag: true,  bill_status: "pending", patient_id: 3, assistant_id: 2)
#   #2
#   Appointment.create(last_call: Date.today-2, code: ["99487","99489"],notes: "Patient is good",bill_notes: "Patients bill Cleared", today_mins: "00:00:00", total_mins: "00:20:45", eligibility: "Eligible", amount: nil, bill_flag: false, bill_status: "pending", patient_id: 3, assistant_id: 2)
#   #3
#   Appointment.create(last_call: Date.today-2, code: ["99487","99489"],notes: "Patient is good",bill_notes: "Patients bill Cleared", today_mins: "00:07:45", total_mins: "00:21:45", eligibility: "Eligible", amount: nil, bill_flag: false, bill_status: "pending", patient_id: 4, assistant_id: 2)
# end